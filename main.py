import requests
from bs4 import BeautifulSoup as Bs
import csv


def get_html(url):
    """
    Get html code from the site with "url" param
    :param url: site url
    :return: html code
    """
    r = requests.get(url)
    return r.text


def refined(s):
    """

    :param s:
    :return:
    """
    r = s.split()[0][1:].replace(',', '')
    return r


def write_csv(data):
    """

    :param data:
    :return:
    """
    with open('plugins.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow((
            data['name'],
            data['url'],
            data['rating']
        ))


def get_data(url):
    """

    :param url:
    :return:
    """
    soup = Bs(get_html(url), 'lxml')
    section = soup.find_all('section')[1]
    plugins = section.find_all('article')
    for plugin in plugins:
        name = plugin.find('h2').text
        url = plugin.find('h2').find('a').get('href')
        rating = plugin.find('span', {'class': 'rating-count'}).text
        rating = refined(rating)
        data = {
            'name': name,
            'url': url,
            'rating': rating
        }
        write_csv(data)


def main():
    url = 'https://wordpress.org/plugins/'
    get_data(url)


if __name__ == '__main__':
    main()
